import java.util.HashMap;
import java.util.Map;

public class CodeDojo {

	public static void main(String[] args) {
		int [] result = twoSum(new int[] {2, 11, 12, 7}, 9);
		System.out.println("result n1: " + result[0] +" n2: " + result[1]);
	}
	
    public static int[] twoSum(int[] nums, int target) {
    	Map<Integer, Integer> numsHash = new HashMap<>();
    	for ( int i = 0; i < nums.length; i++) {
			if(numsHash.containsKey(nums[i])) {
				return new int[] {numsHash.get(nums[i]), i};
			}
			System.out.println("Key: " + (target - nums[i]) + " indice: " + i);
			numsHash.put(target - nums[i], i);
		}
    	return new int[] {};
    }
    
    public boolean isPalindrome(int x) {
        return true;
    }
    
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return new ListNode(0);
    }

}
